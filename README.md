# This package allows preventing indexing of a website during development

### Publish assets
    php artisan vendor:publish --provider="RedRay\IndexPrevention\RedRayIndexPreventionProvider"

### Set .env variable
    ...
    SHOULD_PREVENT_INDEX=true
    ...

### Add the middleware to app/Http/Kernel.php

    protected $middlewareGroups = [
            'web' => [
                ...
                \RedRay\IndexPrevention\Middleware\PreventContentIndex::class
                ...
            ],
        ];
