<?php

return [
    'should_prevent_index' => env('SHOULD_PREVENT_INDEX', true),
];
