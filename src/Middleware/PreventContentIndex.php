<?php

namespace RedRay\IndexPrevention\Middleware;

use Closure;

class PreventContentIndex
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (config('index-prevention.should_prevent_index')) {
            $search = '</head>';
            $replacement = '<meta name="robots" content="noindex, nofollow" />'.$search;

            $response
                ->setContent(str_replace($search, $replacement, $response->getContent()))
                ->header('X-Robots-Tag', 'noindex, nofollow');
        }

        return $response;
    }
}
