<?php

namespace RedRay\IndexPrevention;

use Illuminate\Support\ServiceProvider;

class RedRayIndexPreventionProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerPublishables();
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/index-prevention.php', 'index-prevention');
    }

    protected function registerPublishables()
    {
        $this->publishes([
            __DIR__.'/../config/index-prevention.php' => config_path('index-prevention.php'),
        ], 'config');
    }
}
